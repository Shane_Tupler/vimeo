using System;
using System.IO;
using System.Collections;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using VimeoDotNet;
using VimeoDotNet.Net;
using System.Diagnostics;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;
using System.Collections.Generic;
using System.Linq;

namespace VimeoBatchReplace.Controllers
{
    public class HomeController : Controller
    {
        //"6aee885013ae1955c4210b5f729677a0"
        string accesstoken = "";

        public async Task<ActionResult> Index()
        {
            return View();
        }

        public async Task<ActionResult> Upload(HttpPostedFile[] file)
        {
            string uploadStatus = "";
            try
            {
                string[] addresses = Request.Form.GetValues("fname");
                int i = 0;
                foreach (string fileName in Request.Files)
                {
                    HttpPostedFileBase requestedFile = Request.Files[i];

                    ServicePointManager.Expect100Continue = true;
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

                    accesstoken = Request.Form["token"];

                    VimeoClient vimeoClient = new VimeoClient(accesstoken);

                    var authcheck = await vimeoClient.GetAccountInformationAsync();


                    if (authcheck.Name != null)
                    {
                        IUploadRequest uploadRequest = new UploadRequest();
                        BinaryContent binaryContent = new BinaryContent(requestedFile.InputStream, requestedFile.ContentType);
                        int chunkSize = 0;
                        int contentLength = requestedFile.ContentLength;
                        int temp1 = contentLength / 1024;
                        if (temp1 > 1)
                        {
                            chunkSize = temp1 / 1024;
                            chunkSize /= 10;
                            chunkSize *= 1048576;
                        }
                        else
                        {
                            chunkSize = 1048576;
                        }

                        long replaceAddress = long.Parse(addresses[i]);
                        i++;
                        uploadRequest = await vimeoClient.UploadEntireFileAsync(binaryContent, chunkSize, replaceAddress);
                    }
                }   
            }
            catch (Exception er)
            {
                uploadStatus = "not uploaded:" + er.Message;
                if (er.InnerException != null)
                {
                    uploadStatus += er.InnerException.Message;
                }
            }

            ViewBag.UploadStatus = uploadStatus;

            return View();

        }


        public ActionResult About()
        {
            ViewBag.Message = "Your description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}